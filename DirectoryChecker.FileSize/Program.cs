﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CsvHelper;
using DirectoryChecker.Libraries;

namespace DirectoryChecker.FileSize
{
    class Program
    {
        static void Main(string[] args)
        {


            if (args.Length == 0)
            {
                Console.WriteLine("Please input find directory");
                //Console.ReadKey();
                return;
            }

            var findDir = args[0];


            string outputFile = "FileResult.csv";
            
            
            if(args.Length >= 2)
                outputFile = args[1];


            var fileAnalyzer = new FileAnalyzer();
            //var csvHelper = new CsvHelper();


            var fileDirList = fileAnalyzer.DirSearch(findDir);
            var fileObjList = new List<Libraries.File>();

            Console.WriteLine($@"Checking files...");
            foreach (string oneFile in fileDirList)
            {
                var fileInfo = new FileInfo(oneFile);
                if(fileInfo != null && fileInfo.Exists)
                {
                    var fileObj = new Libraries.File(fileInfo);
                    fileObjList.Add(fileObj);
                }
            }

            var existingFile = new FileInfo(outputFile);
            if (existingFile.Exists)
            {
                Console.WriteLine($@"Deleting existing file...");
                existingFile.Delete();
            }


            Console.WriteLine($@"Writing CSV...");
            using (var writer = new StreamWriter(outputFile))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(fileObjList);
            }


            var newFile = new FileInfo(outputFile);

            
            Console.WriteLine($@"File written to: {outputFile}...");


            //Console.ReadKey();
        }

        
    }
}
