﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DirectoryChecker.Libraries
{
    public class FileAnalyzer
    {
        public List<string> DirSearch(string sDir)
        {
            var returnArr = new List<string>();

            //Console.WriteLine("DirSearch..(" + sDir + ")");
            try
            {

                foreach (string f in Directory.GetFiles(sDir))
                {
                    returnArr.Add(f);
                }

                foreach (string d in Directory.GetDirectories(sDir))
                {
                    var appendArr = this.DirSearch(d);
                    returnArr = returnArr.Concat(appendArr).ToList();
                }
            }
            catch (Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }


            return returnArr;
        }

    }
}
