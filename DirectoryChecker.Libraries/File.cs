﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DirectoryChecker.Libraries
{
    public class File
    {
        public File(FileInfo fileInfo)
        {
            this.Name = fileInfo.Name;
            this.Fullpath = fileInfo.DirectoryName;
            this.Size = fileInfo.Length;
            this.Ext = fileInfo.Extension;
            this.Directory = fileInfo.DirectoryName;
            this.CreateDate = fileInfo.CreationTime;
            this.ModifyDate = fileInfo.LastWriteTime;
            this.AccessDate = fileInfo.LastAccessTime;
        }
        
        public string Directory { get; set; }
        public string Name { get; set; }
        public string Fullpath { get; set; }
        public string Ext { get; set; }
        public long Size { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public DateTime AccessDate { get; set; }
    }
}
