﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using DirectoryChecker.PbasLogChecker.Helpers;
using DirectoryChecker.PbasLogChecker.Models;
using Newtonsoft.Json;

namespace DirectoryChecker.PbasLogChecker
{
    class Program
    {
        static void Main(string[] args)
        {

            // Validation
            if (args.Length == 0)
            {
                Console.WriteLine("Please input find file");
                //Console.ReadKey();
                return;
            }

            var fileName = args[0];
            var targetCsv = args[1];


            var listResult = new List<C5Api>();

            // Read file using StreamReader. Reads file line by line    
            Console.WriteLine("Reading file");
            using (StreamReader file = new StreamReader(fileName))
            {
                int counter = 0;
                int lastRequestId = 0;
                int lastResponseId = 0;
                var lastResponseString = "";
                FocusFindOn focusFindOn = FocusFindOn.FindOnRequest;
                string line;


                while ((line = file.ReadLine()) != null)
                {
                    try
                    {
                        // If begin with at, then skip
                        if (line.StartsWith("at") || line.StartsWith("{\"error_message\":"))
                        {
                            counter++;
                            continue;
                        }

                        // Jika sedang fokus mencari result
                        if (focusFindOn == FocusFindOn.FindOnResult)
                        {

                            // 
                            if (line.StartsWith("Reponse Id"))
                            {
                                lastResponseId = counter;
                            }

                            // Jika response id + 1 kolom artinya cek kolom tersebut jadi json yg baru
                            if (lastResponseId + 1 == counter && line.StartsWith("{\"pefindo_status\":"))
                            {
                                //listResult.Last().Response = JsonConvert.DeserializeObject<C5ApiResponse>(lastResponseString);

                                lastResponseString = lastResponseString.Replace("{\"pefindo_status\":\"1\",\"pefindo_found\":\"0\"}", "");

                                var generatedResponse = JsonConvert.DeserializeObject<C5ApiResponse>(lastResponseString);
                                listResult.Last().Response.Add(generatedResponse);

                                lastResponseString = "";
                                lastResponseString += line;
                            }

                            // Jika response id + 1 kolom artinya cek kolom tersebut jadi json
                            else if (lastResponseId + 1 == counter)
                            {
                                lastResponseString += line;
                            }


                            // Cari terus sampai ketemu request id selanjutnya,
                            // lalu alihkan pencarian ke pencarian request, 
                            // dan convert json ke object
                            if (line.StartsWith("Request Id"))
                            {
                                lastRequestId = counter;
                                focusFindOn = FocusFindOn.FindOnRequest;

                                // Beatify json and compile it
                                //lastResponseString = StringHelper.ExtractFromQuote(lastResponseString);
                                //lastResponseString = JsonConvert.SerializeObject(lastResponseString, Formatting.Indented);

                                //Console.WriteLine(lastResponseString);
                                //Console.ReadKey();

                                lastResponseString = lastResponseString.Replace("{\"pefindo_status\":\"1\",\"pefindo_found\":\"0\"}", "");

                                var generatedResponse = JsonConvert.DeserializeObject<C5ApiResponse>(lastResponseString);


                                listResult.Last().Response.Add(generatedResponse);
                            }

                        }

                        // Jika sedang fokus mencari request
                        if (focusFindOn == FocusFindOn.FindOnRequest)
                        {

                            // Cari request id dan tandai di kolom nomor berapa
                            if (line.StartsWith("Request Id"))
                            {
                                lastRequestId = counter;
                                lastResponseString = "";
                            }

                            // Jika request id + 1 kolom artinya artinya cek kolom tersebut jadi json
                            if (lastRequestId + 1 == counter)
                            {
                                var request = JsonConvert.DeserializeObject<C5ApiRequest>(line);
                                var oneC5api = new C5Api();

                                oneC5api.Request = request;
                                listResult.Add(oneC5api);

                                focusFindOn = FocusFindOn.FindOnResult;
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine($@"Error on line {counter + 1}: {e.Message}, {e.InnerException?.Message}");
                    }
                    counter++;
                }

                // Jika list result yg terakhir kosong, artinya cek kolom terakhir tersebut jadi json
                if (listResult.Count() > 1)
                {

                    //var a = JsonConvert.DeserializeObject(lastResponseString);
                    //lastResponseString = JsonConvert.SerializeObject(a, Formatting.Indented);

                    //Console.WriteLine(lastResponseString);
                    //Console.ReadKey();

                    lastResponseString = lastResponseString.Replace("{\"pefindo_status\":\"1\",\"pefindo_found\":\"0\"}", "");

                    var generatedResponse = JsonConvert.DeserializeObject<C5ApiResponse>(lastResponseString);
                    listResult.Last().Response.Add(generatedResponse);
                }

                file.Close();
                Console.WriteLine($"File has {counter} lines.");
            }

            // Generate Csv
            var resultCsv = new List<ResultCsv>();

            Console.WriteLine("Generating CSV");
            foreach (var oneC5api in listResult)
            {


                var oneRow = new ResultCsv();
                oneRow.Type = "Request";
                oneRow.ReportReffId = oneC5api.Request.report_reff_id;
                oneRow.PefindoId = "";
                oneRow.NoKTP = oneC5api.Request.idnumber;
                oneRow.FullName = oneC5api.Request.fullname;
                oneRow.Dob = oneC5api.Request.dateofbirth;

                resultCsv.Add(oneRow);

                foreach (var oneResponse in oneC5api.Response)
                {


                    if (oneResponse?.infodebitur.Count > 0)
                    {
                        foreach (var oneInfoDebitur in oneResponse.infodebitur)
                        {
                            oneRow = new ResultCsv();
                            oneRow.Type = "Response";
                            oneRow.ReportReffId = oneC5api.Request.report_reff_id;
                            oneRow.PefindoId = oneInfoDebitur.pefindoId;
                            oneRow.NoKTP = oneInfoDebitur.KTP;
                            oneRow.FullName = oneInfoDebitur.fullName;
                            oneRow.Dob = oneInfoDebitur.dateOfBirth;

                            resultCsv.Add(oneRow);
                        }
                    }
                }

            }



            Console.WriteLine($@"Writing CSV...");
            using (var writer = new StreamWriter(targetCsv))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(resultCsv);
            }

            var newFile = new FileInfo(targetCsv);

            Console.WriteLine($@"File written to: {targetCsv}...");

            Console.WriteLine($@"Done");
        }
    }


    public enum FocusFindOn
    {
        FindOnRequest = 0,
        FindOnResult = 1
    }

}
