﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DirectoryChecker.PbasLogChecker.Models
{
    public class ResultCsv
    {
        public string Type { get; set; }
        public string ReportReffId { get; set; }
        public string PefindoId { get; set; }
        public string FullName { get; set; }
        public string NoKTP { get; set; }
        public string Dob { get; set; }

    }
}
