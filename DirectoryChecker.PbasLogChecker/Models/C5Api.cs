﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DirectoryChecker.PbasLogChecker.Models
{

    public class C5Api
    {
        public C5Api()
        {
            this.Response = new List<C5ApiResponse>();
        }
        public C5ApiRequest Request { get; set; }
        public List<C5ApiResponse> Response { get; set; }

    }

    public class C5ApiRequest
    {
        public string report_reff_id { get; set; }
        public string customertype { get; set; }
        public string fullname { get; set; }
        public string dateofbirth { get; set; }
        public string idnumber { get; set; }
        public object npwp { get; set; }
        public string company_name { get; set; }
        public string inquiryreasoncode { get; set; }
        public string inquiryreasontext { get; set; }
        public string correspondence_address { get; set; }
    }

    public class Infodebitur
    {
        public string pefindoId { get; set; }
        public string fullName { get; set; }
        public string dateOfBirth { get; set; }
        public string KTP { get; set; }
        public string motherMaidenName { get; set; }
        public string gender { get; set; }
        public string NPWP { get; set; }
        public string placeOfBirth { get; set; }
        public string addess { get; set; }
        public string city { get; set; }
        public string district { get; set; }
        public string country { get; set; }
        public string postalCode { get; set; }
        public string mobilephone { get; set; }
        public string fixedline { get; set; }
        public string employerName { get; set; }
        public string employerSector { get; set; }
        public string employment { get; set; }
        public int Score { get; set; }
    }

    public class Kredit
    {
        public string pefindoid { get; set; }
        public string fasilitasid { get; set; }
        public string noRekening { get; set; }
        public string ljkKet { get; set; }
        public string jenisKreditPembiayaan { get; set; }
        public string jenisPenggunaan { get; set; }
        public string sektorEkonomiKet { get; set; }
        public string kondisi { get; set; }
        public string kondisiKet { get; set; }
        public string plafon { get; set; }
        public string plafonAwal { get; set; }
        public string outstandingPrincipal { get; set; }
        public string sukuBungaImbalan { get; set; }
        public string tanggalMulai { get; set; }
        public string tanggalJatuhTempo { get; set; }
        public string tanggalAwalKredit { get; set; }
        public string tanggalAkadAwal { get; set; }
        public string noAkadAwal { get; set; }
        public string tanggalAkadAkhir { get; set; }
        public string noAkadAkhir { get; set; }
        public string tanggalUpdate { get; set; }
        public string tanggalKondisi { get; set; }
        public string tanggalRestrukturisasiAkhir { get; set; }
        public string tanggalMacet { get; set; }
        public string kualitas { get; set; }
        public string jumlahHariTunggakan { get; set; }
        public string tahunBulan01 { get; set; }
        public string tahunBulan01Kol { get; set; }
        public string tahunBulan01Ht { get; set; }
        public string tahunBulan02 { get; set; }
        public string tahunBulan02Kol { get; set; }
        public string tahunBulan02Ht { get; set; }
        public string tahunBulan03 { get; set; }
        public string tahunBulan03Kol { get; set; }
        public string tahunBulan03Ht { get; set; }
        public string tahunBulan04 { get; set; }
        public string tahunBulan04Kol { get; set; }
        public string tahunBulan04Ht { get; set; }
        public string tahunBulan05 { get; set; }
        public string tahunBulan05Kol { get; set; }
        public string tahunBulan05Ht { get; set; }
        public string tahunBulan06 { get; set; }
        public string tahunBulan06Kol { get; set; }
        public string tahunBulan06Ht { get; set; }
        public string tahunBulan07 { get; set; }
        public string tahunBulan07Kol { get; set; }
        public string tahunBulan07Ht { get; set; }
        public string tahunBulan08 { get; set; }
        public string tahunBulan08Kol { get; set; }
        public string tahunBulan08Ht { get; set; }
        public string tahunBulan09 { get; set; }
        public string tahunBulan09Kol { get; set; }
        public string tahunBulan09Ht { get; set; }
        public string tahunBulan10 { get; set; }
        public string tahunBulan10Kol { get; set; }
        public string tahunBulan10Ht { get; set; }
        public string tahunBulan11 { get; set; }
        public string tahunBulan11Kol { get; set; }
        public string tahunBulan11Ht { get; set; }
        public string tahunBulan12 { get; set; }
        public string tahunBulan12Kol { get; set; }
        public string tahunBulan12Ht { get; set; }
    }

    public class C5ApiResponse
    {
        public string pefindo_status { get; set; }
        public string pefindo_found { get; set; }
        public List<Infodebitur> infodebitur { get; set; }
        public List<Kredit> kredit { get; set; }
        public List<object> agunan { get; set; }
        public List<object> bpjs { get; set; }
    }
}
