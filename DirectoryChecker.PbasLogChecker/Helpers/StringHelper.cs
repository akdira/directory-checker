﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DirectoryChecker.PbasLogChecker.Helpers
{
    public static class StringHelper
    {

        public static string ExtractFromQuote(string str)
        {
            var output = "";
            var reg = new Regex("\".*?\"");
            var matches = reg.Matches(str);
            foreach (var item in matches)
            {
                output += item;
            }

            return output;

        }

    }
}
